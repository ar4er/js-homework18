const student = {
  firstName: "Peter",
  lastName: "123",
  age: 22,
  skills: {
    css: true,
    html: false,
    javascript: true,
    "play football": true,
  },
  parents: {
    mom: true,
    dad: true,
  },
  sister: {
    first: {
      firstName: "Jane",
    },
    second: {
      firstName: "Sofi",
      kids: {
        first: { firstName: "Jake" },
        second: { firstName: "Anna" },
      },
    },
  },
};

function cloneObject(obj) {
  let studentClone = {};
  for (key in obj) {
    if (key instanceof Object) cloneObject(key);
    studentClone[key] = obj[key];
  }
  return studentClone;
}

const newStudent = cloneObject(student);

console.log(student);
console.log(newStudent);
console.log(newStudent === student);
